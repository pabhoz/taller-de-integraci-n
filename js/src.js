var escena,camaras = {}, models ={},renderer, camaraActiva = null, bounds = {};
var ultiTiempo;
var appW = window.innerWidth;
var appH = window.innerHeight;
var object;
var TECLA = { ARRIBA:false, ABAJO:false, IZQUIERDA:false, DERECHA:false, A:false,W:false,S:false,D:false};
var prevTime = performance.now();
var velocidad = new THREE.Vector3();

var cargador = {
	loadState: false,
	objsToLoad: 0,
	objsLoaded: 0,
	sceneIsReady: false,
	objReady: function(){
		this.objsToLoad--;
		this.objsLoaded++;
		var total = this.objsToLoad+this.objsLoaded;
		var porcentaje = (this.objsLoaded/total)*100;
		/*$("#barraDeCarga div#porcentaje").html(porcentaje+"%");
		$("#barraDeCarga div#carga").css("width",porcentaje+"%");*/
		$("#loadingBar").html(porcentaje+"%");
		if(this.objsToLoad == 0){
			this.loadState = true;
			$('#loadingBar').fadeToggle(4000);
			$("#loadingBar").html("A jugar viejo!");
		}
	},
	addObj: function(){
		this.objsToLoad++;
	}
};

function webGLStart(){
	iniciarEscena();
	$( window ).resize(onWindowResize);
	ultiTiempo = Date.now();
	document.onkeydown = teclaPulsada;
	document.onkeyup = teclaSoltada;
	animarEscena();
}

function iniciarEscena(){
	renderer = new THREE.WebGLRenderer();
	renderer.setClearColor(0x000000, 1);
	renderer.setSize(appW, appH);
	renderer.shadowMapEnabled = true;
	document.body.appendChild(renderer.domElement);

	var camara = new THREE.PerspectiveCamera(45, appW / appH, 1, 10000);
	camara.position.set(1600,1600,-1600);
	camara.lookAt(new THREE.Vector3(0,0,0));

	var tracking = new THREE.PerspectiveCamera(45, appW / appH, 1, 10000);
	tracking.position.set(1600,1600,-1600);
	tracking.lookAt(new THREE.Vector3(0,0,0));

	var fixed = new THREE.PerspectiveCamera(45, appW / appH, 1, 10000);
	fixed.position.set(-600,300,-600);
	fixed.lookAt(new THREE.Vector3(0,0,0));

	var tpersona = new THREE.PerspectiveCamera(45, appW / appH, 1, 10000);
	tpersona.position.set(1600,1600,-1600);
	tpersona.lookAt(new THREE.Vector3(0,0,0));

	var observer = new THREE.PerspectiveCamera(45, appW / appH, 1, 10000);
	observer.position.set(1600,1600,-1600);
	observer.lookAt(new THREE.Vector3(0,0,0));

	camaras.interactive = camara;
	camaras.tracking = tracking;
	camaras.fixed = fixed;
	camaras.tpersona = tpersona;
	camaras.observer = observer;

	camaraActiva = camaras.interactive;

	escena = new THREE.Scene();

	//Iniciar controles de la camara
	controlCamara = new THREE.OrbitControls( camaras.interactive , renderer.domElement );

	//estadisticas
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '10px';
	stats.domElement.style.left = '10px';
	stats.domElement.style.zIndex = '100';
	document.body.appendChild(stats.domElement);

	//Luz AMbiente
	var lambiente = new THREE.AmbientLight(0x3d3c3c);
	escena.add(lambiente);

	//Luz Spotlight
	spotLight = new THREE.SpotLight(0xffcccc, 5, 1500, 20, 10);
	spotLight.position.set( 0, 600, 0 );
	spotLight.castShadow = true;
	escena.add(spotLight);
	escena.add(new THREE.PointLightHelper(spotLight, 1));

	spotLightP2 = new THREE.SpotLight(0xccccff, 5, 1500, 20, 10);
	spotLightP2.position.set( 0, 600, 0 );
	spotLightP2.castShadow = true;
	escena.add(spotLightP2);
	escena.add(new THREE.PointLightHelper(spotLightP2, 1));
	

	//Suelo

	var plano = new THREE.Mesh(
		new THREE.BoxGeometry(1500,1,1500),
		new THREE.MeshPhongMaterial( {color:0x6a6a6a,map: THREE.ImageUtils.loadTexture("textures/kevind.jpg")})
	);
	plano.material.map.repeat.set(10,10);
	plano.material.map.wrapS = plano.material.map.wrapT = THREE.MirroredRepeatWrapping;
	plano.receiveShadow = true;
	plano.position.set(0,-1,0);
	escena.add(plano);

	//Sonidos
		/*sonido1 = new Sound(['sounds/song1.mp3'], 500 ,1,{wireframe:true,scene:escena,pauseOnBlur:true});
		sonido1.setPos(500,0,500);
		//sonido1.play();

		sonido2 = new Sound(['sounds/song2.mp3'], 500 ,1,{wireframe:true,scene:escena,pauseOnBlur:true});
		sonido2.setPos(-500,0,-500);
		//sonido2.play();*/

	//PLAYER
	p1 = new THREE.Mesh(
		new THREE.SphereGeometry(50,50,50),
		new THREE.MeshPhongMaterial({color:0xff0000})
		);
	p1.castShadow = true;
	p1.receiveShadow = true;
	p1.position.y = 50;
	//escena.add(p1);

	//spotLight.target = p1;

	//Entorno
		//cuarto 1
		var cuarto1 = new THREE.Mesh(
			new THREE.BoxGeometry(500,250,50),
			new THREE.MeshLambertMaterial( {color:0x6a6a6a,map: THREE.ImageUtils.loadTexture("textures/kevind.jpg")})
		);
		cuarto1.position.set( 750 - 250, 125 ,750 - 25);
		var p2C1 = new THREE.Mesh(
			new THREE.BoxGeometry(50,250,500),
			new THREE.MeshLambertMaterial( {color:0x6a6a6a,map: THREE.ImageUtils.loadTexture("textures/kevind.jpg")})
		);
		p2C1.position.set( 225, 0 ,-250);
		cuarto1.receiveShadow = true;
		p2C1.receiveShadow = true;
		cuarto1.add(p2C1);
		escena.add(cuarto1);

		//cuarto 2
		var cuarto2 = new THREE.Mesh(
			new THREE.BoxGeometry(500,250,50),
			new THREE.MeshLambertMaterial( {color:0x6a6a6a,map: THREE.ImageUtils.loadTexture("textures/kevind.jpg")})
		);
		cuarto2.position.set( -750 + 250, 125 ,-750 + 25);
		var p2C2 = new THREE.Mesh(
			new THREE.BoxGeometry(50,250,500),
			new THREE.MeshLambertMaterial( {color:0x6a6a6a,map: THREE.ImageUtils.loadTexture("textures/kevind.jpg")})
		);
		p2C2.position.set( 225, 0 ,-250);
		cuarto2.add(p2C2);
		cuarto2.receiveShadow = true;
		p2C2.receiveShadow = true;
		cuarto2.rotation.y = -Math.PI;
		escena.add(cuarto2);

		loadObj('male02','models/male02/male02.obj','models/female02/02_-_Default1noCulling.jpg',new THREE.Vector3(200,0,0),new THREE.Vector3(1,1,1));
		loadObj('mario','models/SuperMario64/Super Mario 64 - Mario.obj','textures/UV_Grid_Sm.jpg',new THREE.Vector3(-200,0,0),new THREE.Vector3(1,1,1));
		loadObjMtl('marioMtl','models/SuperMario64/Super Mario 64 - Mario.obj','models/SuperMario64/Super Mario 64 - Mario.mtl',new THREE.Vector3(200,0,200),new THREE.Vector3(0.3,0.3,0.3));
		loadObjMtl('bowser','models/Bowser/bowser.obj','models/Bowser/bowser.mtl',new THREE.Vector3(-200,25,-200),new THREE.Vector3(6,6,6));
		loadObjMtl('Leona','models/female02/female02.obj','models/female02/female02.mtl',new THREE.Vector3(0,0,0),new THREE.Vector3(1,1,1));
		loadObjMtl('Leon','models/Leon_Kennedy/Leon_Kennedy.obj','models/Leon_Kennedy/Leon_Kennedy.mtl',new THREE.Vector3(0,0,0),new THREE.Vector3(1,1,1));
		loadObjMtl('boo','models/Boo/boo.obj','models/Boo/boo.mtl',new THREE.Vector3(0,300,0),new THREE.Vector3(6,6,6));

	}

	function animarEscena(){
		requestAnimationFrame( animarEscena );

		if(!cargador.loadState && cargador.objsToLoad > 0){
		console.log("Obj Loaded : "+cargador.objsLoaded+" / "+(cargador.objsToLoad+cargador.objsLoaded));
		}else{
			if(!cargador.sceneIsReady){
				cargarModelos();
				console.log("Obj Loaded: "+cargador.objsLoaded+" from "+(cargador.objsToLoad+cargador.objsLoaded));
				console.log("scene Ready");
				cargador.sceneIsReady = true;
				
				p1 = models.marioMtl.obj;
				spotLight.target = p1;
				p2 = models.bowser.obj;
				spotLightP2.target = p2;


			}
			renderEscena();
			actualizarEscena();
		}
	}

	function renderEscena(){
		renderer.render( escena, camaraActiva );
	}

	function cargarModelos(){
		for (var i = 0; i < Object.keys(models).length; i++) {
			var modelo = models[Object.keys(models)[i]];
			if("added" in modelo){
				if(!modelo.added && modelo.obj instanceof THREE.Object3D){
					escena.add(modelo.obj);
					modelo.added = true;
				}
			}
		};
	}

	function actualizarEscena(){

		if(TECLA.ARRIBA){
			if(camaraActiva != camaras.observer){
				move(p1,"straight");
			}else{
		        verticalRot(camaras.observer,"up");
		    }
		}

		if(TECLA.W){ move(p2,"straight"); }

		if(TECLA.ABAJO){
			if(camaraActiva != camaras.observer){
				move(p1,"forward");
			}else{
		        verticalRot(camaras.observer,"down");
		    }
		}

		if(TECLA.S){ move(p2,"forward"); }

		if(TECLA.IZQUIERDA){
			if(camaraActiva != camaras.observer){	
				rotate(p1,"left");
			}else{
		        horizontalRot(camaras.observer,"left");
		    }
		}

		if(TECLA.D){ rotate(p2,"left"); }

		if(TECLA.DERECHA){
			if(camaraActiva != camaras.observer){
				rotate(p1,"right");
			}else{
				horizontalRot(camaras.observer,"right");
			}
		}

		if(TECLA.A){ rotate(p2,"right"); }

		p1.position.y = velocidad.y;

		stats.update();

		//actualización de camaras
		controlCamara.update();
		camaras.tracking.lookAt(p1.position);
		camaras.tpersona.position.copy(p1.position);
		camaras.tpersona.position.y+=300;
		camaras.tpersona.position.x+=300;
		camaras.tpersona.position.z-=300;
		camaras.observer.lookAt(p1.position);


		/*sonido1.update(p1);
		sonido2.update(p1);*/
		

		/*sound1L.lookAt(camara.position);
		sound2L.lookAt(camara.position);*/


	}